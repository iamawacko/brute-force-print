# Brute Force Print

This is a simple and useless library to print in a bruteforce fashion. It was inspired by a Reddit post.

## Example Usage

In your Cargo.toml:
```
brute-force-print = "0.1.0"
```

```
use brute_force_print::bprintln;

fn main() {
	let printable = "Hello, Brute Force!".to_string();
	bprintln(printable);
}
```



